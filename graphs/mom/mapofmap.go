package mom

import (
	"math"

	"git.fractalqb.de/fractalqb/groph"
)

type spmro map[groph.VIdx]interface{}

type SpMoM struct {
	sz groph.VIdx
	ws map[groph.VIdx]spmro
}

func NewSpMoM(vertexNo groph.VIdx, reuse *SpMoM) *SpMoM {
	if reuse == nil {
		return &SpMoM{
			sz: vertexNo,
			ws: make(map[groph.VIdx]spmro),
		}
	} else {
		reuse.Reset(vertexNo)
		return reuse
	}
}

func (g *SpMoM) VertexNo() groph.VIdx { return g.sz }

func (g *SpMoM) Weight(u, v groph.VIdx) interface{} {
	row, ok := g.ws[u]
	if !ok {
		return nil
	}
	return row[v]
}

func (g *SpMoM) SetWeight(u, v groph.VIdx, w interface{}) {
	g.sz = maxSize(g.sz, u, v)
	row, rok := g.ws[u]
	if w == nil {
		if rok {
			delete(row, v)
			if len(row) == 0 {
				delete(g.ws, u)
			}
		}
	} else {
		if !rok {
			row = make(spmro)
			g.ws[u] = row
		}
		row[v] = w
	}
}

func (g *SpMoM) Reset(vertexNo groph.VIdx) {
	g.sz = vertexNo
	g.ws = make(map[groph.VIdx]spmro)
}

func (g *SpMoM) EachNeighbour(v groph.VIdx, do groph.VisitNode) {
	if row, ok := g.ws[v]; ok {
		for n := range row {
			do(n)
		}
	}
}

type spmrof32 map[groph.VIdx]float32

type SpMoMf32 struct {
	sz groph.VIdx
	ws map[groph.VIdx]spmrof32
}

func NewSpMoMf32(vertexNo groph.VIdx, reuse *SpMoMf32) *SpMoMf32 {
	if reuse == nil {
		return &SpMoMf32{
			sz: vertexNo,
			ws: make(map[groph.VIdx]spmrof32),
		}
	} else {
		reuse.Reset(vertexNo)
		return reuse
	}
}

func (g *SpMoMf32) VertexNo() groph.VIdx { return g.sz }

func (g *SpMoMf32) Edge(u, v groph.VIdx) (weight float32) {
	row, ok := g.ws[u]
	if !ok {
		return groph.NaN32()
	}
	weight, ok = row[v]
	if ok {
		return weight
	} else {
		return groph.NaN32()
	}
}

func (g *SpMoMf32) SetEdge(u, v groph.VIdx, weight float32) {
	g.sz = maxSize(g.sz, u, v)
	row, rok := g.ws[u]
	if math.IsNaN(float64(weight)) {
		if rok {
			delete(row, v)
			if len(row) == 0 {
				delete(g.ws, u)
			}
		}
	} else {
		if !rok {
			row = make(spmrof32)
			g.ws[u] = row
		}
		row[v] = weight
	}
}

func (g *SpMoMf32) Weight(u, v groph.VIdx) interface{} {
	tmp := g.Edge(u, v)
	if math.IsNaN(float64(tmp)) {
		return nil
	}
	return tmp
}

func (g *SpMoMf32) SetWeight(u, v groph.VIdx, w interface{}) {
	if w == nil {
		g.SetEdge(u, v, groph.NaN32())
	} else {
		g.SetEdge(u, v, w.(float32))
	}
}

func (g *SpMoMf32) Reset(vertexNo groph.VIdx) {
	g.sz = vertexNo
	g.ws = make(map[groph.VIdx]spmrof32)
}

func (g *SpMoMf32) EachNeighbour(v groph.VIdx, do groph.VisitNode) {
	if row, ok := g.ws[v]; ok {
		for n := range row {
			do(n)
		}
	}
}

func maxSize(currentSize, newIdx1, newIdx2 groph.VIdx) groph.VIdx {
	if s := newIdx1 + 1; s > currentSize {
		if newIdx2 > newIdx1 {
			return newIdx2 + 1
		} else {
			return s
		}
	}
	if s := newIdx2 + 1; s > currentSize {
		return s
	}
	return currentSize
}

type spmroi32 map[groph.VIdx]int32

type SpMoMi32 struct {
	sz groph.VIdx
	ws map[groph.VIdx]spmroi32
}

func NewSpMoMi32(vertexNo groph.VIdx, reuse *SpMoMi32) *SpMoMi32 {
	if reuse == nil {
		return &SpMoMi32{
			sz: vertexNo,
			ws: make(map[groph.VIdx]spmroi32),
		}
	} else {
		reuse.Reset(vertexNo)
		return reuse
	}
}

func (g *SpMoMi32) VertexNo() groph.VIdx { return g.sz }

func (g *SpMoMi32) Edge(u, v groph.VIdx) (w int32, ok bool) {
	row, ok := g.ws[u]
	if !ok {
		return 0, false
	}
	w, ok = row[v]
	if ok {
		return w, true
	} else {
		return 0, false
	}
}

func (g *SpMoMi32) SetEdge(u, v groph.VIdx, weight int32) {
	g.sz = maxSize(g.sz, u, v)
	row, rok := g.ws[u]
	if math.IsNaN(float64(weight)) {
		if rok {
			delete(row, v)
			if len(row) == 0 {
				delete(g.ws, u)
			}
		}
	} else {
		if !rok {
			row = make(spmroi32)
			g.ws[u] = row
		}
		row[v] = weight
	}
}

func (g *SpMoMi32) DelEdge(u, v groph.VIdx) {
	row, ok := g.ws[u]
	if ok {
		delete(row, v)
	}
}

func (g *SpMoMi32) Weight(u, v groph.VIdx) interface{} {
	w, ok := g.Edge(u, v)
	if ok {
		return w
	}
	return nil
}

func (g *SpMoMi32) SetWeight(u, v groph.VIdx, w interface{}) {
	if w == nil {
		g.DelEdge(u, v)
	} else {
		g.SetEdge(u, v, w.(int32))
	}
}

func (g *SpMoMi32) Reset(vertexNo groph.VIdx) {
	g.sz = vertexNo
	g.ws = make(map[groph.VIdx]spmroi32)
}

func (g *SpMoMi32) EachNeighbour(v groph.VIdx, do groph.VisitNode) {
	if row, ok := g.ws[v]; ok {
		for n := range row {
			do(n)
		}
	}
}
